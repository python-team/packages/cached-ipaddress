__version__ = "0.6.0"

from .ipaddress import cached_ip_addresses

__all__ = ["cached_ip_addresses"]
